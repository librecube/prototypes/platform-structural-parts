# Platform - Structural Mounts

Various generic parts for structural mountings.

- 001_panel_solid: A closed panel
- 002_panel_bracket: A panel with mounting bracket
- 003_mounting_bracket: S-shaped mounting bracket
- 004_extension_adapter_frame_A: for length extension of frame A
- 005_extension_adapter_frame_A: for length extension of frame B

## Contribute

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube guidelines](https://gitlab.com/librecube/guidelines).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
